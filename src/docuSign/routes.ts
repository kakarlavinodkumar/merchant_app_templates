import { Router, Request, Response } from "express";
const router = Router();

const path = require('path')
const fs = require('fs-extra')
const docusign = require('docusign-esign')
const demoDocsPath = path.resolve(__dirname, '../../files')
const pdf1File = '1601841826246.pdf'

const worker = async (args: any) => {
    let dsApiClient = new docusign.ApiClient();
    dsApiClient.setBasePath(args.basePath);
    dsApiClient.addDefaultHeader('Authorization', 'Bearer ' + args.accessToken);
    let envelopesApi = new docusign.EnvelopesApi(dsApiClient)
      , results = null;

    // Step 1. Make the envelope request body
    let envelope = await makeEnvelope(args.envelopeArgs)

    // Step 2. call Envelopes::create API method
    // Exceptions will be caught by the calling function
    results = await envelopesApi.createEnvelope(args.accountId, {envelopeDefinition: envelope});

    let envelopeId = results.envelopeId;
    console.log(`Envelope was created. EnvelopeId ${envelopeId}`);

    // Step 3. create the recipient view, the Signing Ceremony
    let viewRequest = await makeRecipientViewRequest(args.envelopeArgs);
    console.log("viewRequest : ", viewRequest);

    // Call the CreateRecipientView API
    // Exceptions will be caught by the calling function
    results = await envelopesApi.createRecipientView(args.accountId, envelopeId,
        {recipientViewRequest: viewRequest});
    console.log("results : ", results);
    return ({envelopeId: envelopeId, redirectUrl: results.url})
}

const makeEnvelope = async (args: any) => {
    let docPdfBytes;
    // read file from a local directory
    // The read could raise an exception if the file is not available!
    docPdfBytes = fs.readFileSync(path.resolve(demoDocsPath, pdf1File));

    // create the envelope definition
    let env = new docusign.EnvelopeDefinition();
    env.emailSubject = 'Please sign this document';

    // add the documents
    let doc1 = new docusign.Document()
      , doc1b64 = Buffer.from(docPdfBytes).toString('base64')
      ;

    doc1.documentBase64 = doc1b64;
    doc1.name = 'Lorem Ipsum'; // can be different from actual file name
    doc1.fileExtension = 'pdf';
    doc1.documentId = '3';

    // The order in the docs array determines the order in the envelope
    env.documents = [doc1];

    // Create a signer recipient to sign the document, identified by name and email
    // We set the clientUserId to enable embedded signing for the recipient
    // We're setting the parameters via the object creation
    let signer1 = docusign.Signer.constructFromObject({
        email: args.signerEmail,
        name: args.signerName,
        clientUserId: args.signerClientId,
        recipientId: 1
    });

    // Create signHere fields (also known as tabs) on the documents,
    // We're using anchor (autoPlace) positioning
    //
    // The DocuSign platform seaches throughout your envelope's
    // documents for matching anchor strings.
    let signHere1 = docusign.SignHere.constructFromObject({
          anchorString: '/sn1/',
          anchorYOffset: '10', anchorUnits: 'pixels',
          anchorXOffset: '20'})
      ;

    // Tabs are set per recipient / signer
    let signer1Tabs = docusign.Tabs.constructFromObject({
      signHereTabs: [signHere1]});
    signer1.tabs = signer1Tabs;

    // Add the recipient to the envelope object
    let recipients = docusign.Recipients.constructFromObject({
      signers: [signer1]});
    env.recipients = recipients;

    // Request that the envelope be sent by setting |status| to "sent".
    // To request that the envelope be created as a draft, set to "created"
    env.status = 'sent';

    return env;
}

function makeRecipientViewRequest(args: any) {
    let viewRequest = new docusign.RecipientViewRequest();

    // Set the url where you want the recipient to go once they are done signing
    // should typically be a callback route somewhere in your app.
    // The query parameter is included as an example of how
    // to save/recover state information during the redirect to
    // the DocuSign signing ceremony. It's usually better to use
    // the session mechanism of your web framework. Query parameters
    // can be changed/spoofed very easily.
    viewRequest.returnUrl = args.dsReturnUrl + "?state=123";

    // How has your app authenticated the user? In addition to your app's
    // authentication, you can include authenticate steps from DocuSign.
    // Eg, SMS authentication
    viewRequest.authenticationMethod = 'none';

    // Recipient information must match embedded recipient info
    // we used to create the envelope.
    viewRequest.email = args.signerEmail;
    viewRequest.userName = args.signerName;
    viewRequest.clientUserId = args.signerClientId;

    // DocuSign recommends that you redirect to DocuSign for the
    // Signing Ceremony. There are multiple ways to save state.
    // To maintain your application's session, use the pingUrl
    // parameter. It causes the DocuSign Signing Ceremony web page
    // (not the DocuSign server) to send pings via AJAX to your
    // app,
    viewRequest.pingFrequency = 600; // seconds
    // NOTE: The pings will only be sent if the pingUrl is an https address
    viewRequest.pingUrl = args.dsPingUrl; // optional setting

    return viewRequest
}

router.post("/send_docs", async (req: Request, res: Response) => {
    try {
        const { filePath, signerEmail, signerName } = req.body;
        const fileElements = filePath.split('/');
        const fileName = fileElements[fileElements.length - 1];

        const args = {
            accessToken: 'eyJ0eXAiOiJNVCIsImFsZyI6IlJTMjU2Iiwia2lkIjoiNjgxODVmZjEtNGU1MS00Y2U5LWFmMWMtNjg5ODEyMjAzMzE3In0.AQoAAAABAAUABwAA3_gnk4vYSAgAAB8cNtaL2EgCAGg5Biob1y5Euvbr9ny5iMUVAAEAAAAYAAEAAAAFAAAADQAkAAAAOTE0NDAxNjQtM2M5ZS00ZDZiLWFkODItZWE5ZjMxMWI1ZWRlIgAkAAAAOTE0NDAxNjQtM2M5ZS00ZDZiLWFkODItZWE5ZjMxMWI1ZWRlNwDAlyzCqXciS6rNrxK7KD72MAAALTAji4vYSA.NKfjq-CpYnM_FW90QJsrT_D8Y3NU5qEp3Z0kuP7oHVPgduZ3mGxC3BpCheae9_K5ohOHDzmyxe9QIYeR5d_KUcHncALplxHBtAmlYPoKVazu4QEDLFyUuzUoQAxmQEDQ7Qd1HUaCZTZXL0FI5Czp04Z7myt77XUOqAhP-PkL5vtzmsOFcsPTeigC35wW2dz5TcBks4TQRk7QB8BlcRToWrk3PVcBZrJ5faOJQfpEJx505Uq9h5062kKmoYGuTPxWa6CxD6igurHQRghnyZhYJyj_OfLacYLHuSFoU_cGB3phLOu6WfvSkF1nz29lEWP_xQzOZBZTMRt0Led1c8Dg0g',
            basePath: 'https://demo.docusign.net/restapi',
            accountId: '712c3b0b-2994-4944-b756-23519c7bcbc7',
            envelopeArgs: {
                signerEmail: 'saroj.kaashyap275@gmail.com',
                signerName: 'sarojk',
                signerClientId: 1001,
                dsReturnUrl: `http://localhost:5000/ds-return`,
                dsPingUrl: `http://localhost:5000/`,
                fileName: fileName
            }
        }

        const docuSignResponse = await worker(args);

        return res.status(200).json({ 'message': 'Successfully Sent for signing docs!!', 'envelopeId': 'success' });
    }
    catch (err) {
        console.log("err : ", err);
        return res.status(500).json({ 'message': 'Server is not responding' });
    }
})

export default router;
