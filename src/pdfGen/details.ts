module.exports = (payload: any) => {
   return `<!doctype html>
     <html>
     
     <head>
         <meta charset="utf-8">
         <title>Merchant App Details : </title>
         <style>
         @font-face {
            font-family: 'SEGOEUI';
            font-style:normal;
            font-weight: 500;
            src: local('SEGOEUI'), url('./fonts/SEGOEUI.TTF') format('truetype');
          }
        @font-face {
            font-family: 'SEGOEUIB';
            font-style:Bold;
            font-weight: 600;
            src: local('SEGOEUIB'), url('./fonts/SEGOEUIB.TTF') format('truetype');
          }
        
        @font-face {
            font-family: 'SEGOEUII';
            font-style:italic;
            font-weight: 400;
            src: local('SEGOEUII'), url('./fonts/SEGOEUII.TTF') format('truetype');
          }
          @font-face {
            font-family: 'SEGOEUIL';
            font-style:normal;
            font-weight: 400;
            src: local('SEGOEUIL'), url('./fonts/SEGOEUIL.TTF') format('truetype');
          }
        @font-face {
            font-family: 'SEGOEUISL';
            font-style:bold;
            font-weight:600;
            src: local('SEGOEUISL'), url('./fonts/SEGOEUISL.TTF') format('truetype');
          }
        @font-face {
            font-family: 'SEGOEUIZ';
            font-style:italic;
            font-weight:500;
            src: local('SEGOEUIZ'), url('./fonts/SEGOEUIZ.TTF') format('truetype');
          }
        @font-face {
            font-family: 'SEGUIBL';
            font-style:normal;
            font-weight:500;
            src: local('SEGUIBL'), url('./fonts/SEGUIBL.TTF') format('truetype');
          }
        @font-face {
            font-family: 'SEGUIBLI';
            font-style:italic;
            font-weight:500;
            src: local('SEGUIBLI'), url('./fonts/SEGUIBLI.TTF') format('truetype');
          }
        @font-face {
            font-family: 'SEGUILI';
            font-style:normal;
            font-weight:400;
            src: local('SEGUILI'), url('./fonts/SEGUILI.TTF') format('truetype');
          }
        @font-face {
            font-family: 'SEGUISB';
            font-style:normal;
            font-weight:400;
            src: local('SEGUISB-SemiBoldItalic'), url('./fonts/SEGUISB.TTF') format('truetype');
          }
        @font-face {
            font-family: 'SEGUISBI';
            font-style:bold;
            font-weight:600;
            src: local('SEGUISBI'), url('./fonts/SEGUISBI.TTF') format('truetype');
          }
        @font-face {
            font-family: 'SEGUISLI';
            font-style:italic;
            font-weight:400;
            src: local('SEGUISLI-Italic'), url('../fonts/SEGUISLI.TTF') format('truetype');
          }        
         html,
         body {
           width: 100%;
           height: 100%;
           margin: 0;
           padding: 0;
           font-family: "SEGOEUI";
         }
         ::-webkit-scrollbar {
           width: 0px;
           background: transparent; /* make scrollbar transparent */
         }
         
         /* Container */
         
         .container {
           width: 100%;
           float: left;
         }
         /* Header Section */
         
         /* .header {
           height: 64px;
           background: #102234;
           width: 100%;
         }
         .logo {
           background: url("./Assets/Images/header-logo.png") no-repeat center center;
           position: absolute;
           width: 15%;
           height: 64px;
           opacity: 1;
           left: 2%;
           } */
         /* Body Section */
           .body-content{
             height:auto;
             width:80%;
             margin:0 auto;
           }
           .center-content{
             margin:0 auto;
             width:76%;
             padding-bottom:30px;
           }
           .form-content{
             float:left;
             width:100%;
             border:1px solid red;
             height:100px;
             clear: both;
           }
           .top-heading{
             color:#2B7FDA;
             font-size: 22px;
             font-family: 'SEGUISB' !important;
             text-transform: uppercase;
             opacity: 1;
             margin-top:16px;
           }
           .centered-heading{
             color:#2B7FDA;
             font-size: 22px;
             font-family: 'SEGUISB' !important;
             text-align:center;
             text-transform: uppercase;
             opacity: 1;
             margin-top:16px;
           }
           .grey-heading{
             height:30px;
             width:100%;
             background-color: #878787;
           }
           .light-grey-heading{
             height:30px;
             width:100%;
             background-color:#EBEBEB;
           }
           .white-heading{
             height:30px;
             width:100%;
             background-color:#F3F3F3;
           }
           .center-text{
             text-align: center;
             color:#000;
             font-size: 11px !important;
             font-family: "SEGOEUIB" !important;
           }
           .height-45{
             height:45px !important;
           }
           .form-heading{
             color: #f4f4f4;
             font-size: 16px;
             margin:0px 80px 0px 10px;
           }
           .form-heading-right{
             float: right ;
             color: #f4f4f4;
             font-size: 16px;
             margin:0px 80px 0px 10px;
           }
           .form-heading:last-child{
             margin-right:10px;
           }
           .form-heading-3{
             width:29%;
             float:left;
             text-align: center;
           }
           .heading-text-black{
             color:#878787;
             margin-left:4%;
           }
           .text-align-center{
             color:#878787;
             padding-left: 42%!important;
           }
           .smalltext-heading{
             color:#f4f4f4;
             font-size: 11px;
           }
           .margin-right-10{
             margin-right: 10px !important;
           }
           .margin-top-50{
             margin-top:50px;
           }
           .margin-top-20{
             margin-top:20px !important;
           }
           .top-container {
             background: #2b7fda;
             height: auto;
             width: 100%;
             margin-top: 10px;
         }
         .top-content{
           margin: 0 18%;
           padding: 20px 5px;
         }
         .bold-text{
           font-size: 12px !important;
           font-family: "SEGOEUIB" !important;
         }
         .text-bolder{
           font-size: 11px !important;
           font-family: "SEGUISB" !important;
         }
         a{
           color:#2B7FDA;
         }
         /* Table */
         td{
           padding-top:10px;
         }
           /* The container */
         .checkbox-container {
           display: block;
           position: relative;
           padding-left: 25px;
           margin-bottom: 10px;
           cursor: pointer;
           font-size: 14px;
           -webkit-user-select: none;
           -moz-user-select: none;
           -ms-user-select: none;
           user-select: none;
         }
         .width-40{
           width:40px !important;
           vertical-align: top;
           padding-right: 5px;
         }
         .width-80{
           width:80px !important;
           vertical-align: top;
           padding-right: 5px;
         }
         .width-82{
           width:82px !important;
           vertical-align: top;
           padding-right: 5px;
         }
         .width-120{
           width:125px !important;
           vertical-align: top;
           padding-right: 5px;
         }
         .width-420{
           width:420 !important;
           vertical-align: top;
           padding-right: 5px;
         }
         .padding-45{
           padding-right:45px;
         }
         .padding-30{
           padding-right:30px;
         }
         .padding-15{
           padding-right:15px;
         }
         .padding-5{
           padding-right:5px;
         }
         .padding-top-25{
           padding-top:25px;
         }
         /* Hide the browser's default checkbox */
         .checkbox-container input {
           position: absolute;
           opacity: 0;
           cursor: pointer;
           height: 0;
           width: 0;
         }
         
         /* Create a custom checkbox */
         .checkmark {
           position: absolute;
           top: 2px;
           left: 0;
           height: 16px;
           width: 16px;
           border:1px solid #ebebeb;
           border-radius:1px;
         }
         
         /* Create the checkmark/indicator (hidden when not checked) */
         .checkmark:after {
           content: "";
           position: absolute;
           display: none;
         }
         
         /* Show the checkmark when checked */
         .checkbox-container input:checked ~ .checkmark:after {
           display: block;
         }
         
         /* Style the checkmark/indicator */
         .checkbox-container .checkmark:after {
           left: 5px;
           width: 4px;
           height: 8px;
           border: solid #2B7FDA;
           border-width: 0 1.5px 1.5px 0;
           -webkit-transform: rotate(45deg);
           -ms-transform: rotate(45deg);
           transform: rotate(45deg);
         }
         .center-content ul {
           list-style: none;
         }
         .center-content ul li {
           font-size: 14px;
         }
         .center-content ul li::before {
           content: "/2022";
           color: #2B7FDA;
           font-weight: bold;
           display: inline-block;
           width: 1.8em;
           font-size:18px;
           margin-left: -1.8em;
         }
         .inline-label{
           width:150px;
           color:#fff;
           font-size:16px;
         }
         .underline-input{
           width:100%;
           font-size: 16px !important;
           font-weight: 400;
           letter-spacing: 1.2px !important;
           height: 28px;
           color: #878787 !important;
           border-bottom: 1px solid #000 !important;
           border: 0;
           outline: 0;
           background: transparent;
           margin-top:20px;
         }
         .large-input:disabled {
           box-shadow: 0 0 0 60px #EBEBEB inset !important;
         }
         .md-input{
           width:95%;
           box-shadow: 0 0 0 60px #fff inset !important;
           font-size: 16px !important;
           font-weight: 400;
           letter-spacing: 1.2px !important;
           height: 28px;
           color: #878787 !important;
           border-radius: 3px !important;
           border: 1px solid #f3f3f3 !important;
         }
         .large-input{
           width:100%;
           box-shadow: 0 0 0 60px #fff inset !important;
           font-size: 16px !important;
           font-weight: 400;
           letter-spacing: 1.2px !important;
           height: 28px;
           color: #878787 !important;
           border-radius: 3px !important;
           border: 1px solid #EBEBEB !important;
         }
         .mediumlarge-input{
           width:47%;
           box-shadow: 0 0 0 60px #fff inset !important;
           font-size: 16px !important;
           font-weight: 400;
           letter-spacing: 1.2px !important;
           height: 28px;
           color: #878787 !important;
           border-radius: 3px !important;
           border: 1px solid #EBEBEB !important;
           margin-bottom: 5px;
         }
         .medium-input{
           width:40%;
           box-shadow: 0 0 0 60px #fff inset !important;
           font-size: 16px !important;
           font-weight: 400;
           letter-spacing: 1.2px !important;
           height: 28px;
           color: #878787 !important;
           border-radius: 3px !important;
           border: 1px solid #EBEBEB !important;
           margin-bottom: 5px;
         }
         .small-input{
           width:30%;
           box-shadow: 0 0 0 60px #fff inset !important;
           font-size: 16px !important;
           font-weight: 400;
           letter-spacing: 1.2px !important;
           height: 28px;
           color: #878787 !important;
           border-radius: 3px !important;
           border: 1px solid #EBEBEB !important;
         }
         .extrasmall-input{
           width:20%;
           box-shadow: 0 0 0 60px #fff inset !important;
           font-size: 16px !important;
           font-weight: 400;
           letter-spacing: 1.2px !important;
           height: 28px;
           color: #878787 !important;
           border-radius: 3px !important;
           border: 1px solid #EBEBEB !important;
         }
         .extrasmall-input-disabled{
           width:20%;
           box-shadow: 0 0 0 60px #EBEBEB inset !important;
           font-size: 16px !important;
           font-weight: 400;
           letter-spacing: 1.2px !important;
           height: 28px;
           color: #878787 !important;
           border-radius: 3px !important;
           border: 1px solid #EBEBEB !important;
         }
         input:focus{
           border: 1px solid #f3f3f3 !important;
           outline:0;
         }
         /* Centered Buttons */
         .flex {
           display: flex;
           justify-content: center;
         }
         
         .flex-item + .flex-item {
           margin-left: 10px;
         }
         .button-inverted{
           background-color: transparent;
           box-shadow: 0 0 0 1px #2b7fda inset !important;
           color: #2b7fda;
           text-align: center;
           width: 120px;
           height: 40px;
           font-size: 16px !important;
           font-weight: 600 !important;
           border-radius: 3px;
           padding: 10px 20px;
           border:none !important;
         }
         /* Form fields  */
         .ui.form {
           position: relative;
           max-width: 100%;
           font-size: 1rem;
         }
         .ui.form .fields {
           display: flex;
           flex-direction: row;
           padding: 5px 10px;
         }
         .ui.form .fields .wide.field {
           width: 6.25%;
           padding-left: .5em;
           padding-right: .5em;
         }
         .ui.form .onehalf.wide.field {
           width: 10%;
           padding-left: .5em;
           padding-right: .5em;
         }
         .ui.form .two.wide.field {
           width: 12.5%!important;
         }
         .ui.form .twohalf.wide.field {
           width: 15.5%!important;
         }
         .ui.form .three.wide.field {
           width: 16.5%!important;
         }
         .ui.form .threehalf.wide.field {
           width: 18.5%!important;
         }
         .ui.form .four.wide.field {
           width: 25%!important;
         }
         .ui.form .fourhalf.wide.field {
           width: 29%!important;
         }
         .ui.form .five.wide.field {
           width: 33.2%!important;
         }
         .ui.form .six.wide.field {
           width: 40%!important;
         }
         .ui.form .eight.wide.field {
           width: 50%!important;
         }
         .ui.form .ten.wide.field {
           width: 62.5%!important;
         }
         .ui.form .fourteen.wide.field {
           width: 87.5%!important;
         }
         .ui.form .sixteen.wide.field {
           width: 100%!important;
         }
         .ui.form .field>label {
           display: inline-block;
           margin: 5px 0px;
           color: rgba(0,0,0,.87);
           font-size: 12px;
           font-weight: 500;
           font-family: 'SEGUISB';
           text-transform: none;
         }
         .ui.form input[type=text]{
           width: 100%;
           vertical-align: top;
         }
         .small-text{
           font-size: 11px;
         }
         .small-text-inside {
           font-size: 11px;
           position: absolute;
           margin-left: 150px;
           margin-top: -25px;
           z-index: 1001;
         }
         .inside-dollar {
           font-size: 12px;
           position: absolute;
           margin-left: -20px;
           margin-top: 7px;
           z-index: 1001;
         }
         .inside-left-dollar {
           font-size: 12px;
           position: absolute;
           margin-left: 6px;
           margin-top: 7px;
           z-index: 1001;
         }
         .margin-left-10{
           margin-left: 10px !important;
         }
         .margin-left-50{
           margin-left: 50px !important;
         }
         .margin-left-28{
           margin-left: 28px !important;
         }
         .margin-left-125{
           margin-left: 125px !important;
         }
         .margin-left-140{
           margin-left: 125px !important;
         }
         .date-icon{
             position: absolute;
             margin-top: -26px;
             margin-left: 28%;
             height: 20px;
             width: 20px;
             background: transparent url("./Assets/Images/date1.svg") no-repeat;
             opacity: 1;
             background-position: center;
             background-size: contain;
         }
         /* Footer Section */
           /* .footerLogoArea {
             background: #102234;
             float: left;
             width: 100%;
           }
           .blockArea {
             width: 65%;
             margin: 0 auto;
             height: 150px;
             padding: 10px;
           }
           .footer-logo {
             float: left;
             position: absolute;
             margin: 15px;
             height: 105px;
             width: 8%;
             background: transparent url("./Assets/Images/footer-logo.png") no-repeat;
             opacity: 1;
             background-position: center;
             background-size: contain;
           }
           .footer-p {
             font-size: 14px;
             color: #fff;
             float: right;
             width: 81%;
             margin-top: 18px;
             margin-right: 20px;
           }
           .footerCardArea {
             float: left;
             width: 100%;
             height: auto;
           }
           .footer-text {
             font-size: 12px;
             color: #000;
             width: 81%;
             text-align: left;
             margin-left: 20px;
           }
           .texthover:hover {
             color: #878787;
           }
           .cards-section {
             height: 45px;
             float: left;
             padding: 20px 0px;
           }
           .cards {
             display: inline;
             width: 100px;
             margin-left: 30px;
             vertical-align: middle;
           }
           .cards:first-child {
             margin-left: 20px;
           }
           .blocked-text {
             float: left;
             width: 100%;
             margin-bottom: 10px;
           }
           .footer-bottom {
             height: 50px;
             width: 100%;
             text-align: center;
             color: #fff;
             background: #000;
             float: left;
             line-height: 3;
           }
           .small-text {
             padding-top: 15px;
             font-size: 11px;
           }
           .bottom-chat-icon {
             margin-left: 8%;
             margin-top: -1.5%;
             height: 45px;
             width: 46px;
             position: absolute;
             background: transparent url("./Assets/Images/footerchaticon.svg") no-repeat;
             opacity: 1;
             background-position: center;
             background-size: contain;
           }
           .footer-grey-button {
             background-color: #53738e !important;
             color:#f4f4f4;
             font-size:11px;
             margin: 10px !important;
             border:none;
             padding:5px 10px;
             border-radius: 2px;
           } */
              
         </style>
     </head>
     
     <body>
         <div class="conatainer">
             <!--html2-->
             <div class="centered-heading">MERCHANT PROCESSING APPLICATION AND AGREEMENT</div>
             <div class="top-container">
                 <div class="top-content">
                     <table width="100%" cellpadding="0" cellspacing="0">
                         <tr>
                             <td><label class="inline-label">Relationship</label></td>
                             <td><input type="text" class="md-input" /></td>
                             <td><label class="inline-label margin-left-50">Association</label></td>
                             <td>
                                 <input type="text" class="md-input" /></td>
                         </tr>
                         <tr>
                             <td><label class="inline-label">Sales Rep Name</label></td>
                             <td><input type="text" class="md-input" /></td>
                             <td><label class="inline-label margin-left-50">Application Date </label></td>
                             <td>
                                 <input type="text" class="md-input"
                                     value=${payload?.app_data_sections?.legal_section?.business_start_date} /></td>
                         </tr>
                     </table>
                 </div>
             </div>
             <div class="top-content">
                 <div class="grey-heading">
                     <span class="form-heading">1. General Information</span>
                     <span class="form-heading">2. Business Location Information</span>
                     <span class="form-heading">3. Business Structure</span>
                 </div>
                 <div class="ui form">
                     <div class="fields">
                         <div class="eight wide field">
                             <label>Client's Business Name (Doing Business As)</label>
                             <input class="large-input" value=${payload?.app_data_sections?.dba_section?.dba_name} />
                         </div>
                         <div class="eight wide field">
                             <label>Client's Corporate/Legal Name (Must match IRS income tax filing)</label>
                             <input class="large-input" value=${payload?.app_data_sections?.legal_section?.legal_name} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="eight wide field">
                             <label>Location Address</label>
                             <input class="large-input" value=${payload?.app_data_sections?.legal_section?.legal_addr_1} />
                         </div>
                         <div class="eight wide field">
                             <label>Corporate Address (If Different Than Location)</label>
                             <input class="large-input" value=${payload?.app_data_sections?.dba_section?.dba_addr_1} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="four wide field">
                             <label>City</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.legal_section?.legal_addr_city} />
                         </div>
                         <div class="two wide field">
                             <label>State</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.legal_section?.legal_addr_state} />
                         </div>
                         <div class="two wide field">
                             <label>Zip</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.legal_section?.legal_addr_postalcode} />
                         </div>
                         <div class="four wide field">
                             <label>City</label>
                             <input class="large-input" value=${payload?.app_data_sections?.dba_section?.dba_addr_city} />
                         </div>
                         <div class="two wide field">
                             <label>State</label>
                             <input class="large-input" value=${payload?.app_data_sections?.dba_section?.dba_addr_state} />
                         </div>
                         <div class="two wide field">
                             <label>Zip</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.dba_section?.dba_addr_postalcode} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="four wide field">
                             <label>Location Phone</label>
                             <input class="large-input" value=${payload?.app_data_sections?.legal_section?.business_phone} />
                         </div>
                         <div class="four wide field">
                             <label>Location Fax</label>
                             <input class="large-input" value=${payload?.app_data_sections?.legal_section?.business_fax} />
                         </div>
                         <div class="four wide field">
                             <label>Contact Name</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.legal_section?.business_contact} />
                         </div>
                         <div class="four wide field">
                             <label>Contact Phone</label>
                             <input class="large-input" value=${payload?.app_data_sections?.legal_section?.business_phone} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="four wide field">
                             <label>Customer Service Phone</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.dba_section?.cust_service_phone} />
                         </div>
                         <div class="four wide field">
                             <label>Prior Security Breach?</label><br />
                             <label class="checkbox-container padding-30">Yes
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.legal_section?.prior_security_breach === 'Yes'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">No
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.legal_section?.prior_security_breach === 'No'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="four wide field">
                             <label>Business Email</label>
                             <input class="large-input" value=${payload?.app_data_sections?.dba_section?.business_email} />
                         </div>
                         <div class="four wide field">
                             <label>D&B#</label>
                             <input class="large-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="eight wide field">
                             <label>Business Website Address</label>
                             <input class="large-input" value=${payload?.app_data_sections?.dba_section?.website_url} />
                         </div>
                         <div class="four wide field">
                             <label>Fed Tax ID </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.app_data_sections?.fed_tax_id_encrypted} />
                         </div>
                         <div class="four wide field">
                             <label>Tax Type</label>
                             <input class="large-input" value=${payload?.app_data_sections?.legal_section?.tax_type} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="four wide field">
                             <label>Multiple locations?</label><br />
                             <label class="checkbox-container padding-30">Yes
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.dba_section?.multiple_locations === 'Yes'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">No
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.dba_section?.multiple_locations === 'No'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="four wide field">
                             <label>Additional location to existing MID</label>
                             <input class="large-input" />
                         </div>
                         <div class="eight wide field">
                             <label>Tax Filing Name</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.legal_section?.tax_filing_name} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="eight wide field">
                             <label>Send retrieval/chargeback requests to</label><br />
                             <label class="checkbox-container padding-30">Corporate Address
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.business_section?.send_chargeback_addr_type === 'Corporate Address'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Location Address
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.business_section?.send_chargeback_addr_type === 'Location Address'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="four wide field">
                             <label>Date Business Started</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.legal_section?.business_start_date} />
                         </div>
                         <div class="four wide field">
                             <label>Length Current Ownership</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.legal_section?.length_of_ownership} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="fourteen wide field">
                             <label>Length Current Ownership</label><br />
                             <label class="checkbox-container padding-30">Corporate Address
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Location Address
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Do not Mail
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label><br />
                             <label class="checkbox-container">Sole Prop
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">Partnership
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">LLC/LLP
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">C Corp
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">S Corp
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">Govt. (Local/State/Federal)
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">501c/Tax Ex.
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="two wide field">
                             <label>State Filing</label><br />
                             <input class="large-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="eight wide field">
                             <label class="checkbox-container">
                                 I certify that I am a foreign entity / nonresident alien. (If checked, please attach IRS
                                 Form W-8.)
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="eight wide field">
                             <p class="small-text">NOTE: Failure to provide accurate information may result in a withholding
                                 of merchant funding per IRS regulations. (See Part IV, Section A.3 of your Program Guide for
                                 further information.)</p>
                         </div>
                     </div>
                 </div>
                 <div class="grey-heading">
                     <span class="form-heading">4. OWNERS/PARTNERS/OFFICERS</span>
                     <span class="form-heading-right">5. TRADE REFERENCE</span>
                 </div>
                 <div class="light-grey-heading">
                     <span class="form-heading heading-text-black">OWNER/PARTNER/OFFICER 1</span>
                     <span class="form-heading heading-text-black">OWNER/PARTNER/OFFICER 2</span>
                     <span class="form-heading heading-text-black">TRADE REFERENCE</span>
                 </div>
                 <div class="ui form">
                     <div class="fields">
                         <div class="five wide field">
                             <label>Name</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[0]?.name} />
                         </div>
                         <div class="five wide field">
                             <label>Name</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[1]?.name} />
                         </div>
                         <div class="five wide field">
                             <label>Business Name</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[2]?.name} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="twohalf wide field">
                             <label>Title</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[0]?.title} />
                         </div>
                         <div class="twohalf wide field">
                             <label>% of Ownership</label
                                 value=${payload?.app_data_sections?.owner_section[0].ownership_pct}>
                             <input class="large-input" />
                         </div>
                         <div class="twohalf wide field">
                             <label>Title</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[1]?.title} />
                         </div>
                         <div class="twohalf wide field">
                             <label>% of Ownership </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[1]?.ownership_pct} />
                         </div>
                         <div class="five wide field">
                             <label>Business Address</label>
                             <input class="large-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label>Home Address</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[0]?.home_addr_1} />
                         </div>
                         <div class="five wide field">
                             <label>Home Address</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[1]?.home_addr_1} />
                         </div>
                         <div class="onehalf wide field">
                             <label>City</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[2]?.home_addr_city} />
                         </div>
                         <div class="onehalf wide field">
                             <label>State </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[2]?.home_addre_state} />
                         </div>
                         <div class="onehalf wide field">
                             <label>Zip</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[2]?.home_addre_postalcode} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="onehalf wide field">
                             <label>City</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[0]?.home_addr_city} />
                         </div>
                         <div class="onehalf wide field">
                             <label>State </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[0]?.home_addre_state} />
                         </div>
                         <div class="onehalf wide field">
                             <label>Zip</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[0]?.home_addre_postalcode} />
                         </div>
                         <div class="onehalf wide field">
                             <label>City</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[1]?.home_addr_city} />
                         </div>
                         <div class="onehalf wide field">
                             <label>State </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[1]?.home_addre_state} />
                         </div>
                         <div class="onehalf wide field">
                             <label>Zip</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[1]?.home_addre_postalcode} />
                         </div>
                         <div class="five wide field">
                             <label>Contact</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[2]?.mobile_number} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="one wide field">
                             <label>Mobile</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[0]?.mobile_number} />
                         </div>
                         <div class="one wide field">
                             <label>DL/ID# </label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[0]?.DL_ID_no} />
                         </div>
                         <div class="onehalf wide field">
                             <label>Issued St.</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[0]?.dl_id_issue_state} />
                         </div>
                         <div class="one wide field">
                             <label>Exp Dt.</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[0]?.dl_id_exp_date} />
                         </div>
                         <div class="one wide field">
                             <label>Mobile </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[1]?.mobile_number} />
                         </div>
                         <div class="one wide field">
                             <label>DL/ID# </label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[1]?.DL_ID_no} />
                         </div>
                         <div class="onehalf wide field">
                             <label>Issued St.</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[1]?.dl_id_issue_state} />
                         </div>
                         <div class="one wide field">
                             <label>Exp Dt.</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[1]?.dl_id_exp_date} />
                         </div>
                         <div class="five wide field">
                             <label>Telephone</label>
                             <input class="large-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="twohalf wide field">
                             <label>Social Security#</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[0]?.ssn_encrypted} />
                         </div>
                         <div class="twohalf wide field">
                             <label>Date of Birth</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[0]?.dob} />
                         </div>
                         <div class="twohalf wide field">
                             <label>Social Security#</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[1]?.ssn_encrypted} />
                         </div>
                         <div class="twohalf wide field">
                             <label>Date of Birth</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[1]?.dob} />
                         </div>
                         <div class="threehalf wide field">
                             <label>Prior Bankrupties?</label><br />
                             <label class="checkbox-container">Yes
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[2]?.prior_bankrupcies === 'Yes'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">No
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[2]?.prior_bankrupcies === 'No'}>
                                 <span class="checkmark"></span>
                             </label><br />
                             <label class="checkbox-container">Business
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">Personal
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="onehalf wide field">
                             <label>Dt. Discharged </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[2]?.bankrupcy_dischage_date} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label>Email ID</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[0].email} />
                         </div>
                         <div class="five wide field">
                             <label>Email ID</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[1]?.email} />
                         </div>
                     </div>
                 </div>
                 <div class="grey-heading">
                     <span class="form-heading">6. NATURE OF BUSINESS</span>
                     <span class="form-heading-right margin-right-10">7. TRANSACTION INFORMATION <span
                             class="smalltext-heading">(see Section 9 American Express)</span></span>
                 </div>
                 <div class="ui form">
                     <div class="fields">
                         <div class="sixteen wide field">
                             <label>Business Type</label><br />
                             <label class="checkbox-container padding-30">Retail
                                 <input type="checkbox"
                                     value${payload?.app_data_sections?.business_section?.business_type === 'Retail'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-15">Restaurant
                                 <input type="checkbox"
                                     value${payload?.app_data_sections?.business_section?.business_type === 'Restaurant'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-5">Government
                                 <input type="checkbox"
                                     value${payload?.app_data_sections?.business_section?.business_type === 'Government'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-15">Lodging
                                 <input type="checkbox"
                                     value${payload?.app_data_sections?.business_section?.business_type === 'Lodging'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Supermarket
                                 <input type="checkbox"
                                     value${payload?.app_data_sections?.business_section?.business_type === 'Supermarket'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Mail/Telephone Order
                                 <input type="checkbox"
                                     value${payload?.app_data_sections?.business_section?.business_type === 'Telephone Order'}>
                                 <span class="checkmark"></span>
                             </label><br />
                             <label class="checkbox-container padding-5">Petroleum
                                 <input type="checkbox"
                                     value${payload?.app_data_sections?.business_section?.business_type === 'Petroleum'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Utilities
                                 <input type="checkbox"
                                     value${payload?.app_data_sections?.business_section?.business_type === 'Utilities'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-15">Healthcare
                                 <input type="checkbox"
                                     value${payload?.app_data_sections?.business_section?.business_type === 'Healthcare'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-5">Education
                                 <input type="checkbox"
                                     value${payload?.app_data_sections?.business_section?.business_type === 'Education'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">QSR
                                 <input type="checkbox"
                                     value${payload?.app_data_sections?.business_section?.business_type === 'QSR'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Charity/Non Profit
                                 <input type="checkbox"
                                     value${payload?.app_data_sections?.business_section?.business_type === 'Charity'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">B2B
                                 <input type="checkbox"
                                     value${payload?.app_data_sections?.business_section?.business_type === 'B2B'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Other
                                 <input type="checkbox"
                                     value${payload?.app_data_sections?.business_section?.business_type === 'Other'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label>Requested Monthly Payment Card Volume</label>
                             <input class="large-input"
                                 value${payload?.app_data_sections?.transaction_section?.monthly_avg_sales} />
                         </div>
                         <div class="five wide field">
                             <label>Card Present Swiped</label>
                             <input class="large-input"
                                 value${payload?.app_data_sections?.transaction_section?.card_types_accepted} />
                         </div>
                         <div class="five wide field">
                             <label>Sales to Consumers</label>
                             <input class="large-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label>Requested Average Payment Card Ticket </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.transaction_section?.avg_ticket_amt} />
                         </div>
                         <div class="five wide field">
                             <label>Card Present Not Swiped </label>
                             <input class="large-input" />
                         </div>
                         <div class="five wide field">
                             <label>Sales to Business </label>
                             <input class="large-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label>Requested Highest Payment Card Ticket</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.transaction_section?.highest_ticket_amt} />
                         </div>
                         <div class="five wide field">
                             <label>MOTO</label>
                             <input class="large-input" />
                         </div>
                         <div class="five wide field">
                             <label>Sales to Govt.</label>
                             <input class="large-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label>Seasonal Merchant?</label><br />
                             <label class="checkbox-container padding-30">Yes
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">No
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="five wide field">
                             <label>Internet (Ecommerce)</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.business_section?.internet_sales_pct} />
                         </div>
                         <div class="five wide field">
                             <label>Days to Delivery</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.transaction_section?.days_to_delivery} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label class="checkbox-container padding-5">J
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-5">F
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-5">M
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-5">A
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-5">M
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-5">J
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-5">J
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-5">A
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-5">S
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-5">O
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-5">N
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-5">D
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="five wide field">
                             <label>Previous Processor </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.business_section?.prev_processor_terminated} />
                         </div>
                         <div class="five wide field">
                             <label>Reason For Leaving</label>
                             <input class="large-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="eight wide field">
                             <label>Description of products or services sold</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.transaction_section?.sales_process_desc} />
                         </div>
                         <div class="eight wide field">
                             <label>Describe your return policy</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.transaction_section?.return_policy_desc} />
                         </div>
                     </div>
                 </div>
                 <div class="grey-heading">
                     <span class="form-heading">8. BANKING ACCOUNT INFORMATION</span>
                 </div>
                 <div class="ui form">
                     <div class="fields">
                         <div class="five wide field">
                             <label>Deposit Bank Name</label>
                             <input class="large-input" value=${payload?.app_data_sections?.bank_section?.bank_name} />
                         </div>
                         <div class="twohalf wide field">
                             <label>Routing#</label>
                             <input class="large-input" value=${payload?.app_data_sections?.bank_section?.bank_routing_no} />
                         </div>
                         <div class="twohalf wide field">
                             <label>Account#</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.bank_section?.bank_account_no_encrypted} />
                         </div>
                         <div class="four wide field">
                             <label>ACH Method</label><br />
                             <label class="checkbox-container padding-5">Yes
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.bank_section?.bank_ach_method === 'Yes'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">No
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.bank_section?.bank_ach_method === 'No'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label>Fees Bank Name</label>
                             <input class="large-input" value=${payload?.app_data_sections?.bank_section?.bank_name} />
                         </div>
                         <div class="twohalf wide field">
                             <label>Routing#</label>
                             <input class="large-input" value=${payload?.app_data_sections?.bank_section?.bank_routing_no} />
                         </div>
                         <div class="twohalf wide field">
                             <label>Account#</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.bank_section?.bank_account_no_encrypted} />
                         </div>
                     </div>
                 </div>
             </div>
     
             <!-- html3 -->
             <div class="top-content">
                 <div class="grey-heading">
                     <span class="form-heading">9. SERVICE ACCEPTANCE AND FEE SCHEDULE</span>
                 </div>
                 <div class="ui form">
                     <div class="fields">
                         <div class="sixteen wide field">
                             <label>Select all card types you wish to accept (See Section 1.9 of the Program Guide for
                                 details
                                 regarding limited acceptance)</label><br />
     
                             <label class="checkbox-container width-82">Visa Credit
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.transaction_section?.card_types_accepted === 'Visa Credit'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container width-82">Visa Non-PIN Debit
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.transaction_section?.card_types_accepted === 'Visa Non-PIN Debit'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container width-82">MasterCard Credit
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.transaction_section?.card_types_accepted === 'MasterCard Credit'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container width-82">MasterCard Non-PIN Debit
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.transaction_section?.card_types_accepted === 'MasterCard Non-PIN Debit'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container width-82">Discover Network
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.transaction_section?.card_types_accepted === 'Discover Network'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container width-82">American Express
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.transaction_section?.card_types_accepted === 'American Express'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container width-82">PIN Debit
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.transaction_section?.card_types_accepted === 'PIN Debit'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <label>Select VIlMClDiscover Network Discount Plan<span class="small-text">(Based on Gross Sales
                                     Volume)</span></label><br />
     
                             <label class="checkbox-container padding-30">Tiered Basic
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Flat Rate
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Pass Through I/C
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <label>Select PinDebit Discount Plan</label><br />
                             <input class="small-input" value=${payload?.app_data_sections?.pricing_details?.pin_debit} />
                             <label>Pin Debit Network Fee Pass-through +</label>
                             <input class="small-input" />
                             <label>% Markup</label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label>Discount Payment Method</label><br />
                             <input class="small-input" />
                             <label>Daily</label>
                             <input class="small-input" />
                             <label>Monthly</label>
                         </div>
                         <div class="five wide field">
                             <label>Assessments</label><br />
                             <input class="extrasmall-input" />
                             <label>Included</label>
                             <input class="extrasmall-input" />
                             <label>Bill Separately</label>
                             <p class="small-text">(If Pass Through I/C - Assessments MUST Bill Separately)</p>
                         </div>
                         <div class="five wide field">
                             <label>Brand Fees</label><br />
                             <input class="extrasmall-input" />
                             <label>Included</label>
                             <input class="extrasmall-input" />
                             <label>Bill Separately</label>
                             <p class="small-text">(If Pass Through I/C - Assessments MUST Bill Separately)</p>
                         </div>
                     </div>
                 </div>
                 <div class="light-grey-heading">
                     <span class="form-heading text-align-center">Discount Fees</span>
                 </div>
                 <div class="ui form">
                     <div class="fields">
                         <div class="onehalf wide field">
                             <label class="bold-text">QUALIFICATION</label>
                         </div>
                         <div class="onehalf wide field">
                             <label class="bold-text">DISC.FEE(%)</label>
                         </div>
                         <div class="onehalf wide field">
                             <label class="bold-text">PER ITEM($)</label>
                         </div>
                         <div class="onehalf wide field">
                             <label class="bold-text">QUALIFICATION</label>
                         </div>
                         <div class="onehalf wide field">
                             <label class="bold-text">DISC. FEE(%)</label>
                         </div>
                         <div class="onehalf wide field">
                             <label class="bold-text">PER ITEM($)</label>
                         </div>
                         <div class="onehalf wide field">
                             <label class="bold-text">QUALIFICATION</label>
                         </div>
                         <div class="onehalf wide field">
                             <label class="bold-text">DISC. FEE(%)</label>
                         </div>
                         <div class="onehalf wide field">
                             <label class="bold-text">PER ITEM($)</label>
                         </div>
                     </div>
                 </div>
                 <div class="light-grey-heading">
                     <div class="light-grey-heading">
                         <span class="form-heading-3 heading-text-black">MasterCard</span>
                         <span class="form-heading-3 heading-text-black">Visa</span>
                         <span class="form-heading-3 heading-text-black">Discover Network</span>
                     </div>
                 </div>
                 <div class="ui form">
                     <div class="fields">
                         <div class="five wide field">
                             <label class="width-120">Credit Qual</label>
                             <input class="extrasmall-input"
                                 value=${payload?.app_data_sections?.pricing_details?.qual_discount} />
                             <input class="extrasmall-input-disabled" placeholder="0.00" disabled />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">Credit Qual</label>
                             <input class="extrasmall-input"
                                 value=${payload?.app_data_sections?.pricing_details?.qual_discount} />
                             <input class="extrasmall-input-disabled" placeholder="0.00" disabled />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">Credit Qual</label>
                             <input class="extrasmall-input"
                                 value=${payload?.app_data_sections?.pricing_details?.qual_discount} />
                             <input class="extrasmall-input-disabled" placeholder="0.00" disabled />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label class="width-120">Credit Mid-Qual</label>
                             <input class="extrasmall-input"
                                 value=${payload?.app_data_sections?.pricing_details?.mid_discount} />
                             <input class="extrasmall-input-disabled" placeholder="0.10" disabled />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">Credit Mid-Qual</label>
                             <input class="extrasmall-input"
                                 value=${payload?.app_data_sections?.pricing_details?.mid_discount} />
                             <input class="extrasmall-input-disabled" placeholder="0.10" disabled />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">Credit Mid-Qual</label>
                             <input class="extrasmall-input"
                                 value=${payload?.app_data_sections?.pricing_details?.mid_discount} />
                             <input class="extrasmall-input-disabled" placeholder="0.10" disabled />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label class="width-120">Credit Non-Qual</label>
                             <input class="extrasmall-input"
                                 value=${payload?.app_data_sections?.pricing_details?.non_discount} />
                             <input class="extrasmall-input-disabled" placeholder="0.10" disabled />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">Credit Non-Qual</label>
                             <input class="extrasmall-input"
                                 value=${payload?.app_data_sections?.pricing_details?.non_discount} />
                             <input class="extrasmall-input-disabled" placeholder="0.10" disabled />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">Credit Non-Qual</label>
                             <input class="extrasmall-input"
                                 value=${payload?.app_data_sections?.pricing_details?.non_discount} />
                             <input class="extrasmall-input-disabled" placeholder="0.10" disabled />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label class="width-120">CheckCard Qual</label>
                             <input class="extrasmall-input" />
                             <input class="extrasmall-input-disabled" placeholder="0.00" disabled />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">CheckCard Qual</label>
                             <input class="extrasmall-input" />
                             <input class="extrasmall-input-disabled" placeholder="0.00" disabled />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">CheckCard Qual</label>
                             <input class="extrasmall-input" />
                             <input class="extrasmall-input-disabled" placeholder="0.00" disabled />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label class="width-120">CheckCard Mid-Qual</label>
                             <input class="extrasmall-input" />
                             <input class="extrasmall-input-disabled" placeholder="0.10" disabled />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">CheckCard Mid-Qual</label>
                             <input class="extrasmall-input" />
                             <input class="extrasmall-input-disabled" placeholder="0.10" disabled />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">CheckCard Mid-Qual</label>
                             <input class="extrasmall-input" />
                             <input class="extrasmall-input-disabled" placeholder="0.10" disabled />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label class="width-120">CheckCard Non-Qual</label>
                             <input class="extrasmall-input" />
                             <input class="extrasmall-input-disabled" placeholder="0.10" disabled />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">CheckCard Non-Qual</label>
                             <input class="extrasmall-input" />
                             <input class="extrasmall-input-disabled" placeholder="0.10" disabled />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">CheckCard Non-Qual</label>
                             <input class="extrasmall-input" />
                             <input class="extrasmall-input-disabled" placeholder="0.10" disabled />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label class="width-120">Credit Pass Through IC</label>
                             <input class="extrasmall-input" />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">Credit Pass Through IC</label>
                             <input class="extrasmall-input" />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">Credit Pass Through IC</label>
                             <input class="extrasmall-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label class="width-120">CheckCard Pass<br />Through IC </label>
                             <input class="extrasmall-input" />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">CheckCard Pass<br />Through IC </label>
                             <input class="extrasmall-input" />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">CheckCard Pass<br />Through IC </label>
                             <input class="extrasmall-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label class="width-120">ERR</label>
                             <input class="extrasmall-input" />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">ERR</label>
                             <input class="extrasmall-input" />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">ERR</label>
                             <input class="extrasmall-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label class="width-120">Voyager</label>
                             <input class="medium-input" />
                             <label class="width-120"></label>
                             <input class="medium-input" />
                         </div>
                         <div class="ten wide field">
                             <span class="small-text">All applicable Association fees will be passed through to the merchant
                                 at the
                                 applicable costs assigned by the Association. Fees include, but are not
                                 limited to, Visa’s APF, Misuse of Authorization Fee, Zero Floor Limit Fee, Acquirer ISA Fee,
                                 and
                                 MasterCard’s NABU Fee, Acquirer Support Fee,
                                 Cross Border Fee, and Discover IPF, ISF, Data Usage fee, Amex Net Work Fee et al.
                             </span>
                         </div>
                     </div>
                 </div>
                 <div class="light-grey-heading">
                     <span class="form-heading text-align-center">American Express</span>
                 </div>
                 <div class="white-heading">
                     <span class="form-heading-3 heading-text-black">&nbsp;</span>
                     <span class="form-heading-3 heading-text-black">OptBlue<sup>SM</sup></span>
                     <span class="form-heading-3 heading-text-black">Amex Direct</span>
                 </div>
                 <div class="light-grey-heading height-45">
                     <div class="ui form">
                         <div class="fields">
                             <div class="twohalf wide field"><label>QUALIFICATION</label></div>
                             <div class="one wide field"><label>DISC.<br />FEE(%)</label></div>
                             <div class="one wide field"><label>PER <br />ITEM($)</label></div>
                         </div>
                     </div>
                 </div>
                 <div class="ui form">
                     <div class="fields">
                         <div class="five wide field">
                             <label class="width-120">Credit Qual</label>
                             <input class="extrasmall-input" />
                             <input class="extrasmall-input" />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">Monthly Card Volume</label>
                             <input class="mediumlarge-input" />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">Order New</label>
                             <input class="mediumlarge-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label class="width-120">Credit Mid-Qual</label>
                             <input class="extrasmall-input" />
                             <input class="extrasmall-input" />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">Average Card Ticket </label>
                             <input class="mediumlarge-input" />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">Use Existing</label>
                             <input class="mediumlarge-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label class="width-120">Credit Non-Qual</label>
                             <input class="extrasmall-input" />
                             <input class="extrasmall-input" />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">Highest Card Ticket</label>
                             <input class="mediumlarge-input" />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">CAP #</label>
                             <input class="mediumlarge-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label class="width-120">Credit Pass Through IC</label>
                             <input class="extrasmall-input" />
                             <input class="extrasmall-input" />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">SE #</label>
                             <input class="mediumlarge-input" />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">Existing SE #</label>
                             <input class="mediumlarge-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label class="width-120">ERR</label>
                             <input class="extrasmall-input" />
                             <input class="extrasmall-input" />
                         </div>
                         <div class="five wide field">
                             <label class="width-120">Prior Security Breach?</label><br />
                             <label class="checkbox-container padding-30">Tiered Basic
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">Flat Rate
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">Pass Through I/C
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">Enhanced Recover Reduction (ERR)
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="five wide field">
                             <span class="small-text">Monthly flat fee of $7.95 or Discount Rate may apply</span>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <span class="small-text">Fee applies to all American Express Programs.</span>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <span class="small-text">**0.30% downgrade will be charged by American Express for transactions
                                 whenever
                                 a CNP or Card Not Present Charge occurs. CNP means a Charge for which the Card is not
                                 presented at
                                 the point of purchase (e.g., Charges by mail, telephone, fax or the Internet). Note: The CNP
                                 Fee is
                                 applicable to transactions made on all American Express Cards, including Prepaid
                                 Cards.</span>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <span class="small-text">An Inbound fee of 0.40% will be applied on any Charge made using a
                                 Card,
                                 including Prepaid Cards, that was issued outside the United States (as used herein, the
                                 United
                                 States does not include Puerto Rico, the U.S. Virgin Islands and other U.S. territories and
                                 possessions). This fee is applicable to all industries listed in Appendix B, except
                                 Education in the
                                 following categories: Sporting & Recreation Camps (MCC 7032), Elementary & Secondary Schools
                                 (MCC
                                 8211), Colleges, Universities, Professional Schools (MCC 8220), and Child Care Services (MCC
                                 8351).</span>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <label class="checkbox-container small-text">By checking this box, you opt out of receiving
                                 future
                                 commercial marketing communications from American Express.
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <span class="small-text margin-left-28">Note that you may continue to receive marketing
                                 communications
                                 while American Express updates its records to reflect your choice. Opting out of commercial
                                 marketing communications will not preclude you from receiving important transactional or
                                 relationship messages from American Express.</span>
                         </div>
                     </div>
                 </div>
                 <div class="light-grey-heading">
                     <div class="ui form">
                         <div class="fields">
                             <div class="eight wide field"><span class="text-align-center">Authorization Fees</span></div>
                             <div class="eight wide field"><span class="text-align-center">Monthly Fees</span></div>
                         </div>
                     </div>
                 </div>
                 <div class="ui form">
                     <div class="fields">
                         <div class="four wide field">
                             <label>Visa/MC/Discover Network</label>
                             <input class="large-input" value=${payload?.app_data_sections?.pricing_details?.card_type} />
                         </div>
                         <div class="four wide field">
                             <label>Electronic AVS</label>
                             <input class="large-input" value=${payload?.app_data_sections?.pricing_details?.avs} />
                         </div>
                         <div class="four wide field">
                             <label>Monthly Minimum</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.pricing_details?.monthly_minimum} />
                         </div>
                         <div class="four wide field">
                             <label>Industry Compliance</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.pricing_details?.industry_compliance} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="four wide field">
                             <label>Amex/Fleet/Other</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.transaction_section?.amex_type} />
                         </div>
                         <div class="four wide field">
                             <label>Voice Authorization</label>
                             <input class="large-input" value=${payload?.app_data_sections?.pricing_details?.voice_auth} />
                         </div>
                         <div class="four wide field">
                             <label>Wireless Fee</label>
                             <input class="large-input" value=${payload?.app_data_sections?.pricing_details?.wireless_fee} />
                         </div>
                         <div class="four wide field">
                             <label>Monthly Service Fee</label>
                             <input class="large-input" value=${payload?.app_data_sections?.pricing_details?.monthly_fee} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="four wide field">
                             <label>Pin Debit Authorization</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.pricing_details?.pin_debit_auth} />
                         </div>
                         <div class="four wide field">
                             <label>Voice AVS</label>
                             <input class="large-input" value=${payload?.app_data_sections?.pricing_details?.voice_auth} />
                         </div>
                         <div class="four wide field">
                             <label>PIN Debit Fee</label>
                             <input class="large-input" />
                         </div>
                         <div class="four wide field">
                             <label>Misc Monthly Fee</label>
                             <input class="large-input" value=${payload?.app_data_sections?.pricing_details?.monthly_fee} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="eight wide field">
                             <label>EBT Authorization</label>
                             <input class="large-input" value=${payload?.app_data_sections?.pricing_details?.ebt_auth} />
                         </div>
                         <div class="four wide field">
                             <label>Industry Non-Compliance<span class="small-text">(Up $24.95)</span></label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.pricing_details?.industry_compliance} />
                         </div>
                         <div class="four wide field">
                             <span class="small-text">(if applicable per Section 4.8 of the Merchant Program Guide)</span>
                         </div>
                     </div>
                 </div>
                 <div class="light-grey-heading">
                     <div class="ui form">
                         <div class="fields">
                             <div class="eight wide field"><span class="text-align-center">Miscellaneous Fees</span></div>
                             <div class="eight wide field"><span class="text-align-center">MX Merchant Fees</span></div>
                         </div>
                     </div>
                 </div>
                 <div class="ui form">
                     <div class="fields">
                         <div class="four wide field">
                             <label>Sales Transaction Fee<span class="small-text">(All card types)</span></label>
                             <input class="large-input" />
                             <span class="small-text-inside">(per Item)</span>
                         </div>
                         <div class="four wide field">
                             <label>Chargeback Fee</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.pricing_details?.charge_back_fee} />
                             <span class="small-text-inside margin-left-125">(per Occurence)</span>
                         </div>
                         <div class="eight wide field">
                             <label>MX Merchant Monthly Fee</label>
                             <input class="large-input" value=${payload?.app_data_sections?.pricing_details?.monthly_fee} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="four wide field">
                             <label>Retrieval Fee<span class="small-text">(All card types)</span></label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.pricing_details?.retreival_fee} />
                             <span class="small-text-inside margin-left-125">(per Occurence)</span>
                         </div>
                         <div class="four wide field">
                             <label>Return Transaction Fee</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.pricing_details?.return_transaction_fee} />
                             <span class="small-text-inside">(per Item)</span>
                         </div>
                         <div class="eight wide field">
                             <label>MX Merchant Plan</label>
                             <br />
                             <label class="checkbox-container padding-30">Reporting
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Basic
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Plus
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="four wide field">
                             <label>Batch Fee</label>
                             <input class="large-input" value=${payload?.app_data_sections?.pricing_details?.batch_fee} />
                             <span class="small-text-inside">(per Item)</span>
                         </div>
                         <div class="four wide field">
                             <label>Annual Fee</label>
                             <input class="large-input" value=${payload?.app_data_sections?.pricing_details?.annual_fee} />
                         </div>
                         <div class="eight wide field">
                             <label>MX Gateway Transaction Fee</label>
                             <br />
                             <label class="checkbox-container padding-30">Premium
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Enterprise
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="four wide field">
                             <label>ACH Reject</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.pricing_details?.ach_reject_fee} />
                             <span class="small-text-inside margin-left-125">(per Occurence)</span>
                         </div>
                         <div class="four wide field">
                             <label>Annual Fee Bill Month</label>
                             <input class="large-input" />
                         </div>
                         <div class="eight wide field">
                             <label>Bill to</label>
                             <br />
                             <label class="checkbox-container padding-30">Statement
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Separate
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                 </div>
             </div>
     
             <!--html4-->
             <div class="top-content">
                 <div class="grey-heading">
                     <span class="form-heading">10. OTHER CARD TYPES</span>
                 </div>
                 <div class="ui form">
                     <div class="fields">
                         <div class="five wide field">
                             <label>Accept EBT</label><br />
                             <label class="checkbox-container width-82">Yes
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.pricing_details?.accept_ebt === 'Yes'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container width-82">No
                                 <input type="checkbox" value=${payload?.app_data_sections?.pricing_details?.accept_ebt === 'No'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="five wide field">
                             <label>Order Voyager</label><br />
                             <label class="checkbox-container width-82">Yes
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container width-82">No <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="five wide field">
                             <label>Order ACH/Check Services</label><br />
                             <label class="checkbox-container width-82">Yes
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container width-82">No <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label><br />
                             <span class="small-text">(Must attach addendum with app copy)</span>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label>Accept EBT Cash Benefit</label><br />
                             <label class="checkbox-container width-82">Yes
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container width-82">No
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="five wide field">
                             <label>Order Wright Express</label><br />
                             <label class="checkbox-container width-82">Yes
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container width-82">No
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label><br />
                             <span class="small-text">(Must attach Wright Express application and Debranding
                                 letter with app
                                 copy)</span>
                         </div>
                         <div class="five wide field">
                             <label>Order Gift Card</label><br />
                             <label class="checkbox-container width-82">Yes
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container width-82">No
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label><br />
                             <span class="small-text">(Must attach addendum with app copy)</span>
                         </div>
                     </div>
                 </div>
                 <div class="grey-heading">
                     <span class="form-heading">11a. EQUIPMENT l PROCESSING METHOD</span>
                 </div>
                 <div class="ui form">
                     <div class="fields">
                         <div class="sixteen wide field">
                             <label>Application Type</label><br />
                             <label class="checkbox-container padding-15">Retail
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.equipment_section?.equipment_type === 'Retail'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-15">Retail w/ Tip
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.equipment_section?.equipment_type === 'Retail w/ Tip'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-15">MOTO
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.equipment_section?.equipment_type === 'MOTO'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-15">Restaurant w/ Tip
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.equipment_section?.equipment_type === 'Restaurant w/ Tip'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-15">Quick Serve Restaurant (no tip)
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.equipment_section?.equipment_type === 'Quick Serve Restaurant'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-15">Hotel
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.equipment_section?.equipment_type === 'Hotel'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">Auto Rental
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.equipment_section?.equipment_type === 'Auto Rental'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                 </div>
                 <div class="light-grey-heading">
                     <div class="ui form">
                         <div class="fields">
                             <div class="threehalf wide field"><label>Terminal Features</label></div>
                             <div class="one wide field"><label>Yes</label></div>
                             <div class="one wide field"><label>No</label></div>
                             <div class="threehalf wide field"><label>&nbsp;</label></div>
                             <div class="one wide field"><label>Yes</label></div>
                             <div class="one wide field"><label>No</label></div>
                             <div class="threehalf wide field"><label>&nbsp;</label></div>
                             <div class="one wide field"><label>Yes</label></div>
                             <div class="one wide field"><label>No</label></div>
                         </div>
                     </div>
                 </div>
                 <div class="ui form">
                     <div class="fields">
                         <div class="threehalf wide field"><label>Fraud Check (last 4-digits)</label></div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span></label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span></label>
                         </div>
                         <div class="threehalf wide field"><label>Purchasing Card</label></div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span></label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span></label>
                         </div>
                         <div class="threehalf wide field"><label>Invoice/Purchase Order #</label></div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span></label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span></label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="threehalf wide field"><label>AVS + CVV2</label></div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox" value=${payload?.app_data_sections?.pricing_details?.avs === 'Yes'}>
                                 <span class="checkmark"></span></label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox" value=${payload?.app_data_sections?.pricing_details?.avs === 'No'}>
                                 <span class="checkmark"></span></label>
                         </div>
                         <div class="threehalf wide field"><label>Server/Clerk #</label></div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span></label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span></label>
                         </div>
                         <div class="threehalf wide field"><label>Auto Close</label></div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span></label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span></label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="ten wide field"> &nbsp;</div>
                         <div class="five wide field">
                             <label class="width-120 margin-left-28">If yes, time?</label>
                             <input class="small-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label>Accept EBT Cash Benefit</label><br />
                             <label class="checkbox-container padding-30">Yes
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.pricing_details?.accept_ebt === 'Yes'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">No
                                 <input type="checkbox" value=${payload?.app_data_sections?.pricing_details?.accept_ebt === 'No'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="five wide field">
                             <label>Wireless?</label><br />
                             <label class="checkbox-container padding-30">Yes
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.pricing_details?.wireless_fee === 'Yes'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">No
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.pricing_details?.wireless_fee === 'No'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="five wide field">
                             <label>Special Requests<span class="small-text">(Multi-Mid, Dial 9,
                                     etc)</span></label>
                             <input class="large-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label>If yes, Terminal Serial </label>
                             <input class="large-input" />
                         </div>
                         <div class="five wide field">
                             <label>Wireless Info: MAN/Serial </label>
                             <input class="large-input" />
                         </div>
                         <div class="five wide field">
                             <label>SIM Card Number </label>
                             <input class="large-input" />
                         </div>
                     </div>
                 </div>
                 <div class="light-grey-heading">
                     <div class="ui form">
                         <div class="fields">
                             <div class="fourhalf wide field" style="padding-left: 6%"><label>TYPE OF
                                     EQUIPMENT</label></div>
                             <div class="threehalf wide field"><label>PRODUCT NAME</label></div>
                             <div class="threehalf wide field"><label>QUANTITY</label></div>
                             <div class="four wide field"><label>DEPLOYMENT</label></div>
                         </div>
                     </div>
                 </div>
                 <div class="ui form">
                     <div class="fields">
                         <div class="one wide field"><label>Terminal</label></div>
                         <div class="one wide field"><label>Pinpad</label></div>
                         <div class="one wide field"><label>Printer</label></div>
                         <div class="one wide field"><label>VAR*</label></div>
                         <div class="twohalf wide field">&nbsp;</div>
                         <div class="four wide field">&nbsp;</div>
                         <div class="one wide field"><label>Existing</label></div>
                         <div class="one wide field"><label>Agent</label></div>
                         <div class="twohalf wide field"><label>New Order <br />(attach order form)</label></div>
                     </div>
                     <div class="fields">
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">Terminal
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.equipment_section?.equipment_type === 'Terminal'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">Pinpad
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.equipment_section?.equipment_type === 'Pinpad'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">Printer
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.equipment_section?.equipment_type === 'Printer'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">VAR*
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.equipment_section?.equipment_type === 'VAR*'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="twohalf wide field">
                             <label style="vertical-align:top">
                                 <input class="large-input"
                                     value=${payload?.app_data_sections?.equipment_section?.existing_equip_name} />
                             </label>
     
                         </div>
                         <div class="threehalf wide field">
                             <input class="large-input" />
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="twohalf wide field">
                             <input class="large-input" />
                         </div>
                         <div class="threehalf wide field">
                             <input class="large-input" />
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="twohalf wide field">
                             <input class="large-input" />
                         </div>
                         <div class="threehalf wide field">
                             <input class="large-input" />
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="twohalf wide field">
                             <input class="large-input" />
                         </div>
                         <div class="threehalf wide field">
                             <input class="large-input" />
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                         <div class="one wide field">
                             <label class="checkbox-container" style="vertical-align:top">
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="eight wide field">
                             <label>*Manufacturerlproductlversion of PClInternet Software</label>
                             <input class="large-input" />
                         </div>
                         <div class="eight wide field">
                             <label>Do you use any third party to store, process, or transmit cardholder
                                 data?</label>
                             <label class="checkbox-container padding-30">Yes
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">No
                                 <input type="checkbox">
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <label>If yes, give name/address:</label>
                             <input class="large-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="three wide field">
                             <label>ORDER LEASE </label>
                             <input class="large-input" />
                         </div>
                         <div class="four wide field">
                             <label>Lease Company</label>
                             <input class="large-input" />
                         </div>
                         <div class="three wide field">
                             <label>Lease Term</label>
                             <input class="large-input" />
                         </div>
                         <div class="three wide field">
                             <label>Mos.</label>
                             <input class="large-input" />
                         </div>
                         <div class="four wide field">
                             <label>Annual Tax Handling Fee</label>
                             <input class="large-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="four wide field">
                             <label>Total Monthly Lease Charge </label>
                             <input class="large-input" />
                         </div>
                         <div class="ten wide field">
                             <label class="padding-top-25">w/o taxes, lates fees, or other charges that may apply
                                 - See Lease
                                 Agreement for details.</label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="fourteen wide field">
                             <label class="padding-top-25">This is a NON-CANCELLABLE lease for the full term
                                 indicated</label>
                         </div>
                         <div class="twohalf wide field">
                             <label>Client's initials</label>
                             <input class="large-input" />
                         </div>
                     </div>
                 </div>
                 <div class="grey-heading">
                     <span class="form-heading">11b. CARD NOT PRESENT INFORMATION</span>
                 </div>
                 <div class="ui form">
                     <h5>If you process more than 30% of your bankcard transactions, or volume, without swiping
                         andlor examining
                         the credit card, please complete this section and provide the information requested.
                     </h5>
                     <div class="fields">
                         <div class="sixteenwide field">
                             <label>1. Please submit your Product catalog; brochures; promotional materials; a
                                 current price
                                 list; and a copy of your service agreement with card holder if applicable. If on
                                 the Internet,
                                 please include screen-prints of your website address if your site is not yet
                                 active.</label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteenwide field">
                             <label>2. If Internet, please check your type of business:</label><br />
                             <label class="checkbox-container padding-30">Web Hosting
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.business_section?.business_type === 'Web Hosting'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Domain Registration
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.business_section?.business_type === 'Domain Registration'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Web page Design
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.business_section?.business_type === 'Web page Design'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Auction
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.business_section?.business_type === 'Auction'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Internet Service Gateway
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.business_section?.business_type === 'Internet Service Gateway'}>
                                 <span class="checkmark"></span>
                             </label><br />
                             <label class="checkbox-container padding-30">Selling Digital Service
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.business_section?.business_type === 'Selling Digital Service'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Advertisement
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.business_section?.business_type === 'Advertisement'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Selling Hard Goods
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.business_section?.business_type === 'Selling Hard Goods'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">other
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.business_section?.business_type === 'other'}>
                                 <span class="checkmark"></span>
                             </label>
                             <input class="extrasmall-input" /><br />
                             <span class="small-text">If using the Internet, list encryption method, vendor, and
                                 controls used to
                                 secure transaction information</span>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <label>3. How will the product be advertised or promoted?</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.transaction_section?.advertising_methods} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <label>4. Billing Methods: (Check all that apply)</label><br />
                             <label class="checkbox-container">Monthly
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.transaction_section?.advertising_methods === 'Monthly'}>
                                 <span class="checkmark"></span>
                             </label>
                             <input class="extrasmall-input width-80" />
                             <span class="inside-dollar">%</span>
                             <label class="checkbox-container">Yearly
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.transaction_section?.advertising_methods === 'Yearly'}>
                                 <span class="checkmark"></span>
                             </label>
                             <input class="extrasmall-input width-80" />
                             <span class="inside-dollar">%</span>
                             <label class="checkbox-container">Quarterly
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.transaction_section?.advertising_methods === 'Quarterly'}>
                                 <span class="checkmark"></span>
                             </label>
                             <input class="extrasmall-input width-80" />
                             <span class="inside-dollar">%</span>
                             <label class="checkbox-container">One Time
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.transaction_section?.advertising_methods === 'One Time'}>
                                 <span class="checkmark"></span>
                             </label>
                             <input class="extrasmall-input width-80" />
                             <span class="inside-dollar">%</span>
                             <label class="checkbox-container">Hourly
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.transaction_section?.advertising_methods === 'Hourly'}>
                                 <span class="checkmark"></span>
                             </label>
                             <input class="extrasmall-input width-80" />
                             <span class="inside-dollar">%</span>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <label>5. List the name(s) and address(es) of the vendor(s) from which supplies are
                                 purchased.</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.business_section?.vendor_names} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <label>6. Who performs product/service fulfillment? If direct from vendor, please
                                 provide Vendor
                                 Name, address and phone number in full:</label>
                             <input class="large-input" />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <label>7. Please describe how a sale takes place from beginning of order until
                                 completion of
                                 fulfillment</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.transaction_section?.sales_process_desc} />
                         </div>
                     </div>
                 </div>
             </div>
     
             <!--html 7-->
             <div class="centered-heading" style="margin: 20px 0px;">PART I: CONFIRMATION PAGE</div>
             <div class="top-container">
                 <div class="top-content">
                     <table width="100%" cellpadding="0" cellspacing="0">
                         <tr>
                             <td style="width: 140px;"><label class="inline-label">PROCESSOR</label></td>
                             <td><label class="inline-label">Name</label></td>
                             <td colspan="3"><input type="text" class="large-input"
                                     value=${payload?.merchant_prescreen?.processor_name} /></td>
                         </tr>
                         <tr>
                             <td><label class="inline-label">INFORMATION</label></td>
                             <td><label class="inline-label">Address</label></td>
                             <td colspan="3"><input type="text" class="large-input"
                                     value=${payload?.app_data_sections?.dba_section?.dba_addr_1} /></td>
                         </tr>
                         <tr>
                             <td><label class="inline-label">&nbsp;</label></td>
                             <td><label class="inline-label">URL</label></td>
                             <td><input type="text" class="md-input"
                                     value=${payload?.app_data_sections?.dba_section?.website_url} /></td>
                             <td><label class="inline-label margin-left-28">Customer Service #</label></td>
                             <td><input type="text" class="md-input margin-left-10"
                                     value=${payload?.app_data_sections?.dba_section?.cust_service_phone} /></td>
                         </tr>
                     </table>
                 </div>
             </div>
             <div class="top-content">
                 <div class="ui form">
                     <div class="fields">
                         <div class="sixteen wide field">
                             <span class="bold-text">Merchant Beneficial Ownership and Management Information Certification:
                             </span>
                             <span class="text-bolder">The following information and certifications concerning beneficial
                                 ownership, and the identification of beneficial owner(s), of the Merchant identified in the
                                 Merchant Application referenced below, must be provided for the Merchant if a legal entity
                                 (legal entity includes a corporation, limited liability company or other entity that is
                                 formed
                                 by filing of a public document with a Secretary of State or similar office, a general
                                 partnership, and any similar business entity formed in the United States). (This form need
                                 not
                                 be used for a Merchant identified in the Merchant Application as a “sole proprietor” or
                                 “sole
                                 proprietorship”, provided the prescribed forms of Merchant
                                 Application including any Patriot Act/customer identification forms and taxpayer
                                 identification/withholding forms included therein or prescribed for use therewith reflect
                                 such
                                 sole proprietorship status and are completed and executed by such sole proprietor and the
                                 Processor’s representative.) The beneficial ownership/management information and
                                 certification
                                 in this form is in addition to, not a substitute for, the information and certifications
                                 regarding the Merchant legal entity required elsewhere in the prescribed form of Merchant
                                 Application including any other Patriot Act/customer identification forms and taxpayer
                                 identification/withholding forms included therein or prescribed for use therewith. </span>
                             <span class="bold-text">Notice: To help the government fight the funding of terrorism and money
                                 laundering activities, the USA Patriot Act requires all financial institutions to obtain,
                                 verify
                                 and record information that identifies each person (including business entities) who opens
                                 an
                                 account. What this means for you: When you open an account we will ask for your name,
                                 address,
                                 date of birth, and other information that will allow us to identity you. We may also ask to
                                 see
                                 your driver’s license or other identifying documents. In some instances we may use outside
                                 sources to confirm the information. </span>
                             <span class="text-bolder">Priority Payment System’s privacy policy can be found at
                                 www.prioritypaymentsystems.com.</span>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <span class="bold-text">Section 1: Merchant Application Information </span>
                             <span class="text-bolder">(Must match information in Merchant Application):</span>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="eight wide field">
                             <label> Date Application Signed (by Authorized Signer named below)</label>
                             <input class="large-input" value=${payload?.merchant_app_signed_ts} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="five wide field">
                             <label>Merchant Legal Name</label>
                             <input class="large-input" value=${payload?.app_data_sections?.legal_section?.legal_name} />
                         </div>
                         <div class="five wide field">
                             <label>Merchant Federal Tax ID </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.legal_section?.fed_tax_id_encrypted} />
                         </div>
                         <div class="five wide field">
                             <label>Merchant State of formation/Incorporation</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.legal_section?.state_incorporated} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="ten wide field">
                             <label>Merchant Address</label>
                             <input class="large-input" value=${payload?.app_data_sections?.legal_section?.legal_addr_1} />
                         </div>
                         <div class="five wide field">
                             <label>Merchant Entity Type </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.legal_section?.legal_entity_type} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <span class="bold-text">Section 2: Beneficial Ownership and Management Information. </span>
                             <span class="text-bolder">Provide the information below on each individual who directly or
                                 indirectly, through any contract, arrangement, understanding, relationship or otherwise,
                                 owns
                                 25% or more of the equity interests of the Merchant legal entity identified above. If the
                                 total
                                 ownership of those individuals does not exceed 50% of the equity interests of the Merchant,
                                 provide the information below on additional beneficial owners so that the total ownership
                                 interests of individuals for which information is provided below exceeds 50%. (Use extra
                                 copies
                                 if needed.) Information must be provided for one individual with significant responsibility
                                 for
                                 managing the legal entity listed in Section 1, a “Control Prong”. Examples of a Control
                                 Prong
                                 include, but are not limited to: Chief Executive Officer, Chief Financial Officer, Chief
                                 Operating Officer, Managing Member, General Partner, President, Vice President or Treasurer.
                                 If
                                 no other Beneficial Owner identified below is identified in the right column as the Control
                                 Prong, the Control Prong section below must be completed.</span>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="six wide field">
                             <label class="bold-text">Beneficial Owner Legal Name</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[0]?.name} />
                         </div>
                         <div class="six wide field">
                             <label class="bold-text">Title </label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[0]?.title} />
                         </div>
                         <div class="threehalf wide field">
                             <label class="bold-text">Legal Entity Ownership</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[0]?.ownership_pct} />
                             <span class="small-text-inside margin-left-140">%</span>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="six wide field">
                             <label>Individual’s Home (Street) Address (No P.O. Box)</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[0]?.home_addr_1} />
                         </div>
                         <div class="six wide field">
                             <label>City, State, Zip </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[0]?.home_addr_city} />
                         </div>
                         <div class="threehalf wide field">
                             <label>Date of Birth</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[0]?.dob} />
                             <span class="date-icon margin-left-140"></span>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="six wide field">
                             <label>Individual has a Social Security Number or Individual Taxpayer Identification Number
                                 issued
                                 by US Government?</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[0]?.DL_ID_no} />
                         </div>
                         <div class="threehalf wide field">
                             <label>Social Security No. (SSN)/Individual Taxpayer </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[0]?.ssn_encrypted} />
                         </div>
                         <div class="threehalf wide field">
                             <label>Taxpayer Identification No. (ITIN)</label>
                             <input class="large-input" />
                         </div>
                         <div class="threehalf wide field">
                             <label>Control Prong? </label><br />
                             <label class="checkbox-container margin-top-20">Yes
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[0]?.control_prong === 'Yes'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="six wide field">
                             <label>ID Type:* </label><br />
                             <label class="checkbox-container">Driver’s License
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[0]?.dl_id_doc_type === 'Driver’s License'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">Passport
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[0]?.dl_id_doc_type === 'Passport'}>
                                 <span class="checkmark"></span>
                             </label><br />
                             <label class="checkbox-container">Resident Alien ID
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[0]?.dl_id_doc_type === 'Resident Alien ID'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">Other State photo ID showing residence
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[0]?.dl_id_doc_type === 'Other State photo ID showing residence'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Other ID
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[0]?.dl_id_doc_type === 'Other ID'}>
                                 <span class="checkmark"></span>
                             </label>
                             <input class="extrasmall-input width-82" />
                         </div>
                         <div class="threehalf wide field">
                             <label>State/Country of Issuance</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[0]?.dl_id_issue_state} />
                         </div>
                         <div class="two wide field">
                             <label>Date Issued</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[0]?.dl_id_issue_date} />
                         </div>
                         <div class="one wide field">
                             <label>Expiration</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[0]?.dl_id_exp_date} />
                         </div>
                         <div class="threehalf wide field">
                             <label>Number on ID</label><br />
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[0]?.DL_ID_no} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="six wide field">
                             <label class="bold-text">Beneficial Owner Legal Name</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[1]?.name} />
                         </div>
                         <div class="six wide field">
                             <label class="bold-text">Title </label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[1]?.title} />
                         </div>
                         <div class="threehalf wide field">
                             <label class="bold-text">Legal Entity Ownership</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[1]?.ownership_pct} />
                             <span class="small-text-inside margin-left-140">%</span>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="six wide field">
                             <label>Individual’s Home (Street) Address (No P.O. Box)</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[1]?.home_addr_1} />
                         </div>
                         <div class="six wide field">
                             <label>City, State, Zip </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[1]?.home_addr_city} />
                         </div>
                         <div class="threehalf wide field">
                             <label>Date of Birth</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[1]?.dob} />
                             <span class="date-icon margin-left-140"></span>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="six wide field">
                             <label>Individual has a Social Security Number or Individual Taxpayer Identification Number
                                 issued
                                 by US Government?</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[1]?.DL_ID_no} />
                         </div>
                         <div class="threehalf wide field">
                             <label>Social Security No. (SSN)/Individual Taxpayer </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[1]?.ssn_encrypted} />
                         </div>
                         <div class="threehalf wide field">
                             <label>Identification No. (ITIN): State/Country of Issuance</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[1]?.dl_id_issue_state} />
                         </div>
                         <div class="threehalf wide field">
                             <label>Control Prong? </label><br />
                             <label class="checkbox-container margin-top-20">Yes
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[1]?.control_prong === 'Yes'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="six wide field">
                             <label>ID Type:* </label><br />
                             <label class="checkbox-container">Driver’s License
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[1]?.dl_id_doc_type === 'Driver’s License'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">Passport
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[1]?.dl_id_doc_type === 'Passport'}>
                                 <span class="checkmark"></span>
                             </label><br />
                             <label class="checkbox-container">Resident Alien ID
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[1]?.dl_id_doc_type === 'Resident Alien ID'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">Other State photo ID showing residence
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[1]?.dl_id_doc_type === 'Other State photo ID showing residence'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Other ID
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[1]?.dl_id_doc_type === 'Other ID'}>
                                 <span class="checkmark"></span>
                             </label>
                             <input class="extrasmall-input width-82" />
                         </div>
                         <div class="threehalf wide field">
                             <label>State/Country of Issuance</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[1]?.dl_id_issue_state} />
                         </div>
                         <div class="two wide field">
                             <label>Date Issued</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[1]?.dl_id_issue_date} />
                         </div>
                         <div class="one wide field">
                             <label>Expiration</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[1]?.dl_id_exp_date} />
                         </div>
                         <div class="threehalf wide field">
                             <label>Number on ID</label><br />
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[1]?.DL_ID_no} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="eight wide field">
                             <label class="checkbox-container bold-text"">Control Prong (and/or Additional Beneficial Owner)
                                         <input type=" checkbox"
                                 value=${payload?.app_data_sections?.owner_section[1]?.control_prong === 'Yes'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="six wide field">
                             <label class="bold-text">Beneficial Owner Legal Name</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[2]?.name} />
                         </div>
                         <div class="six wide field">
                             <label class="bold-text">Title </label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[2]?.title} />
                         </div>
                         <div class="threehalf wide field">
                             <label class="bold-text">Legal Entity Ownership</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[2]?.ownership_pct} />
                             <span class="small-text-inside margin-left-140">%</span>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="six wide field">
                             <label>Individual’s Home (Street) Address (No P.O. Box)</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[2]?.home_addr_1} />
                         </div>
                         <div class="six wide field">
                             <label>City, State, Zip </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[2]?.home_addr_city} />
                         </div>
                         <div class="threehalf wide field">
                             <label>Date of Birth</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[2]?.dob} />
                             <span class="date-icon margin-left-140"></span>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="six wide field">
                             <label>Individual has a Social Security Number or Individual Taxpayer Identification Number
                                 issued
                                 by US Government?</label>
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[2]?.DL_ID_no} />
                         </div>
                         <div class="threehalf wide field">
                             <label>Social Security No. (SSN)/Individual Taxpayer </label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[2]?.ssn_encrypted} />
                         </div>
                         <div class="threehalf wide field">
                             <label>Identification No. (ITIN): State/Country of Issuance</label>
                             <input class="large-input" />
                         </div>
                         <div class="threehalf wide field">
                             <label>Control Prong? </label><br />
                             <label class="checkbox-container margin-top-20">Yes
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[2]?.control_prong === 'Yes'}>
                                 <span class="checkmark"></span>
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="six wide field">
                             <label>ID Type:* </label><br />
                             <label class="checkbox-container">Driver’s License
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[1]?.dl_id_doc_type === 'Driver’s License'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">Passport
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[1]?.dl_id_doc_type === 'Passport'}>
                                 <span class="checkmark"></span>
                             </label><br />
                             <label class="checkbox-container">Resident Alien ID
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[1]?.dl_id_doc_type === 'Resident Alien ID'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container">Other State photo ID showing residence
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[1]?.dl_id_doc_type === 'Other State photo ID showing residence'}>
                                 <span class="checkmark"></span>
                             </label>
                             <label class="checkbox-container padding-30">Other ID
                                 <input type="checkbox"
                                     value=${payload?.app_data_sections?.owner_section[1]?.dl_id_doc_type === 'Other ID'}>
                                 <span class="checkmark"></span>
                             </label>
                             <input class="extrasmall-input width-82" />
                         </div>
                         <div class="threehalf wide field">
                             <label>State/Country of Issuance</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[1]?.dl_id_issue_state} />
                         </div>
                         <div class="two wide field">
                             <label>Date Issued</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[2]?.dl_id_issue_date} />
                         </div>
                         <div class="one wide field">
                             <label>Expiration</label>
                             <input class="large-input"
                                 value=${payload?.app_data_sections?.owner_section[2]?.dl_id_exp_date} />
                         </div>
                         <div class="threehalf wide field">
                             <label>Number on ID</label><br />
                             <input class="large-input" value=${payload?.app_data_sections?.owner_section[2]?.DL_ID_no} />
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <label>* For US persons provide unexpired Driver’s License unless there is none; for non-US
                                 persons
                                 ID Type may be unexpired Resident Alien ID, or Passport/Other ID± and Country of issuance.
                             </label>
                             <label>± Specify type of “Other ID”, which may be any other unexpired government-issued document
                                 evidencing nationality or residence and bearing a photograph or similar safeguard.
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="sixteen wide field">
                             <span class="bold-text">Certifications and Signatures:</span><br />
                             <label>
                                 The undersigned Authorized Signer, listed above as a Beneficial Owner or Control Prong, who
                                 has
                                 signed the Merchant Application on behalf of the Merchant, hereby certifies that he/she is
                                 authorized to open accounts for the Merchant at financial institutions, that all information
                                 provided above about the Merchant legal entity is complete and correct and that, to the best
                                 of
                                 his/her knowledge, all information provided above about each individual listed above is
                                 complete
                                 and correct and there is no individual who directly or indirectly owns 25% or more of the
                                 Merchant legal entity’s equity interests whose information is not provided above. The
                                 Authorized
                                 Signer and the Processor’s Representative, each hereby certify that the information listed
                                 above
                                 regarding the identity and the identification document of each individual listed above, is
                                 complete and correct and was personally observed on the indicated document.
                             </label>
                         </div>
                     </div>
                     <div class="fields">
                         <div class="twohalf wide field">
                             <input class="underline-input" />
                             <label>Authorized Signer Signature</label>
                         </div>
                         <div class="twohalf wide field">
                             <input class="underline-input" />
                             <label>Date Signed</label>
                         </div>
                         <div class="twohalf wide field">
                             <input class="underline-input" />
                             <label>Authorized Signer Printed Name</label>
                         </div>
     
                         <div class="twohalf wide field">
                             <input class="underline-input" />
                             <label>Processor’s Rep. Printed Name</label>
                         </div>
                         <div class="twohalf wide field">
                             <input class="underline-input" />
                             <label>Date Signed</label>
                         </div>
                         <div class="twohalf wide field">
                             <input class="underline-input" />
                             <label>Processor’s Rep. Printed Name</label>
                         </div>
                     </div>
                 </div>
             </div>
     
             <!--html 8-->
             <div class="body-content">
                 <div class="center-content">
                     <div class="centered-heading" style="margin: 20px 0px;">MERCHANT RESERVE ACKNOWLEDGMENT AND ACCEPTANCE
                     </div>
                     <div class="ui form">
                         <div class="fields">
                             <div class="sixteen wide field">
                                 <label>The undersigned Merchant hereby acknowledges and accepts that as a condition of
                                     approval or continuance of his/her merchant credit card processing account(s)
                                     (identified below), and for other good and valuable consideration the receipt and
                                     sufficiency of which are hereby acknowledged, Priority Payment Systems LLC and its
                                     agents, including a processing Bank designated by Priority have the authority to
                                     establish a reserve account in accordance with Section 25.1 of the Merchant Processing
                                     Application and Agreement which includes the Program Guide Terms and Conditions (“MPA”)
                                     as follows:</label>
                             </div>
                         </div>
                         <div class="fields">
                             <div class="sixteen wide field">
                                 <label>1. The reserve account will be established by:</label><br />
                                 <label class="margin-left-10">A. A certified check made payable to Priority Payment Systems
                                     LLC in the amount of </label>&nbsp;&nbsp;
                                 <span class="inside-left-dollar">$</span>
                                 <input class="extrasmall-input width-82" />
                                 <input class="extrasmall-input width-82" /><label>(initials)</label><br />
                                 <label class="margin-left-10">B. Withholding</label>&nbsp;<input
                                     class="extrasmall-input width-82" /><span class="inside-dollar">%</span>&nbsp;<label>
                                     each gross deposit </label>&nbsp;
                                 <input class="extrasmall-input width-82" /><label>(initials)</label>
                             </div>
                         </div>
                         <div class="fields">
                             <div class="sixteen wide field">
                                 <label>2. The reserve account will be used to offset any amounts owed by Merchant under the
                                     MPA. Merchant will forward to Priority funds to replenish the reserve account if any
                                     funds are debited from it.</label>
                             </div>
                         </div>
                         <div class="fields">
                             <div class="sixteen wide field">
                                 <label>3. The balance of the reserve account, if any, will be returned to Merchant up to 270
                                     days after termination of the MPA or Merchant’s last transmission of sales drafts,
                                     whichever is later.</label><br />
                                 <label>Merchant acknowledges that if there is any conflict between the terms of this
                                     merchant reserve acknowledgment and acceptance and the terms of the MPA, the terms of
                                     the MPA will govern.</label>
                                 <label>Acknowledged and Agreed to this </label>&nbsp;
                                 <input class="extrasmall-input width-82" />&nbsp;<label>day of</label>&nbsp;
                                 <input class="extrasmall-input width-82" />&nbsp;<label>, 20</label>&nbsp;
                                 <input class="extrasmall-input width-40" />
                             </div>
                         </div>
                         <div class="fields">
                             <div class="four wide field">
                                 <input class="underline-input" />
                                 <label>Signature</label>
                             </div>
                         </div>
                         <div class="fields">
                             <div class="five wide field">
                                 <label>Name & Title</label>
                                 <input class="large-input" value=${payload?.merchant_name} />
                             </div>
                             <div class="five wide field">
                                 <label>Merchant Legal Business Name</label>
                                 <input class="large-input" value=${payload?.app_data_sections?.legal_section?.legal_name} />
                             </div>
                             <div class="five wide field">
                                 <label>DBA Name</label>
                                 <input class="large-input" value=${payload?.app_data_sections?.dba_section?.dba_name} />
                             </div>
                         </div>
                         <div class="fields">
                             <div class="five wide field">
                                 <label>Merchant Account No.: (if applicable)</label>
                                 <input class="large-input"
                                     value=${payload?.app_data_sections?.bank_section?.bank_account_no_encrypted} />
                             </div>
                             <div class="five wide field">
                                 <label>Accepted</label>
                                 <input class="large-input" />
                             </div>
                         </div>
     
                     </div>
                 </div>
             </div>
     
             <!--html 9-->
             <div class="body-content">
                 <div class="center-content">
                     <div class="centered-heading" style="margin: 20px 0px;">COMPLIANCE MONITORING SET UP FORM</div>
                     <div class="ui form">
                         <div class="fields">
                             <div class="sixteen wide field">
                                 <label class="bold-text">Merchant/Business Information</label>
                             </div>
                         </div>
                         <div class="fields">
                             <div class="sixteen wide field">
                                 <label>Business Name</label>
                                 <input class="large-input" value=${payload?.app_data_sections?.legal_section?.legal_name} />
                             </div>
                         </div>
                         <div class="fields">
                             <div class="eight wide field">
                                 <label>Address Line 1</label>
                                 <input class="large-input"
                                     value=${payload?.app_data_sections?.legal_section?.legal_addr_1} />
                             </div>
                             <div class="eight wide field">
                                 <label>Address Line 2</label>
                                 <input class="large-input" />
                             </div>
                         </div>
                         <div class="fields">
                             <div class="four wide field">
                                 <label>City</label>
                                 <input class="large-input"
                                     value=${payload?.app_data_sections?.legal_section?.legal_addr_city} />
                             </div>
                             <div class="four wide field">
                                 <label>State</label>
                                 <input class="large-input"
                                     value=${payload?.app_data_sections?.legal_section?.legal_addr_state} />
                             </div>
                             <div class="four wide field">
                                 <label>Zip</label>
                                 <input class="large-input"
                                     value=${payload?.app_data_sections?.legal_section?.legal_addr_postalcode} />
                             </div>
                             <div class="four wide field">
                                 <label>Phone</label>
                                 <input class="large-input"
                                     value=${payload?.app_data_sections?.legal_section?.business_phone} />
                             </div>
                         </div>
                         <div class="fields">
                             <div class="sixteen wide field">
                                 <label class="bold-text">Billing Information</label>
                             </div>
                         </div>
                         <div class="fields">
                             <div class="five wide field">
                                 <label>One-Time Set Up Fee</label>
                                 <input class="large-input" disabled value="$ 200" />
                             </div>
                             <div class="five wide field">
                                 <label>Monthly Fee</label>
                                 <input class="large-input" disabled value="$ 200" />
                             </div>
                         </div>
                         <div class="fields">
                             <div class="sixteen wide field">
                                 <label class="bold-text">Owner/Principal Information</label>
                             </div>
                         </div>
                         <div class="fields">
                             <div class="five wide field">
                                 <label>Contact Name</label>
                                 <input class="large-input" value=${payload?.app_data_sections?.owner_section[0]?.name} />
                             </div>
                             <div class="five wide field">
                                 <label>Email Address</label>
                                 <input class="large-input" value=${payload?.app_data_sections?.owner_section[0]?.email} />
                             </div>
                             <div class="five wide field">
                                 <label>Cell</label>
                                 <input class="large-input" />
                             </div>
                         </div>
                         <div class="fields">
                             <div class="sixteen wide field">
                                 <label class="bold-text">Authorization </label>
                             </div>
                         </div>
                         <div class="fields">
                             <div class="five wide field">
                                 <input class="underline-input" />
                                 <label>Signature</label>
                             </div>
                             <div class="five wide field">
                                 <label>Date</label>
                                 <input class="large-input"
                                     value=${payload?.app_data_sections?.owner_section[0]?.bankrupcy_dischage_date} />
                                 <span class="date-icon"></span>
                             </div>
                             <div class="five wide field">
                                 <label>Print Name</label>
                                 <input class="large-input" value=${payload?.app_data_sections?.owner_section[0]?.name} />
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
     
             <!--html 10-->
             <div class="body-content">
                 <div class="center-content">
                     <div class="centered-heading" style="margin: 20px 0px;">Third Party Vendor List</div>
                     <div class="ui form">
                         <div class="fields">
                             <div class="sixteen wide field">
                                 <label class="bold-text">Customer Records Management (Please provide agreements)</label>
                             </div>
                         </div>
                         <div class="fields">
                             <div class="eight wide field">
                                 <label>Name</label>
                                 <input class="large-input" />
                             </div>
                             <div class="eight wide field">
                                 <label>Phone</label>
                                 <input class="large-input" />
                             </div>
                         </div>
                         <div class="fields">
                             <div class="sixteen wide field">
                                 <label class="bold-text">Fulfillment Center (Please provide agreements)</label>
                             </div>
                         </div>
                         <div class="fields">
                             <div class="eight wide field">
                                 <label>Name</label>
                                 <input class="large-input" />
                             </div>
                             <div class="eight wide field">
                                 <label>Phone</label>
                                 <input class="large-input" />
                             </div>
                         </div>
                         <div class="fields">
                             <div class="sixteen wide field">
                                 <label>**lf the fulfillment center has not been approved by Trinity Payments, please
                                     provide: </label>
                             </div>
                         </div>
                         <div class="fields">
                             <div class="four wide field">
                                 <label>Street</label>
                                 <input class="large-input" />
                             </div>
                             <div class="four wide field">
                                 <label>City</label>
                                 <input class="large-input" />
                             </div>
                             <div class="four wide field">
                                 <label>State</label>
                                 <input class="large-input" />
                             </div>
                             <div class="four wide field">
                                 <label>Zip Code</label>
                                 <input class="large-input" />
                             </div>
                         </div>
                         <div class="fields">
                             <div class="sixteen wide field">
                                 <label class="bold-text">Chargeback Management Partner</label>
                             </div>
                         </div>
                         <div class="fields">
                             <div class="eight wide field">
                                 <label>Name</label>
                                 <input class="large-input"
                                     value=${payload?.app_data_sections?.business_section?.chargeback_company_name} />
                             </div>
                             <div class="eight wide field">
                                 <label>Phone</label>
                                 <input class="large-input" />
                             </div>
                         </div>
                         <div class="fields">
                             <div class="sixteen wide field">
                                 <label class="bold-text">Primary Affiliate Marketing Partner</label>
                             </div>
                         </div>
                         <div class="fields">
                             <div class="eight wide field">
                                 <label>Name</label>
                                 <input class="large-input"
                                     value=${payload?.app_data_sections?.business_section?.affliate_marketing_partner_name} />
                             </div>
                             <div class="eight wide field">
                                 <label>Phone</label>
                                 <input class="large-input" />
                             </div>
                         </div>
                         <div class="fields">
                             <div class="sixteen wide field">
                                 <label class="bold-text">Call Center</label>
                             </div>
                         </div>
                         <div class="fields">
                             <div class="eight wide field">
                                 <label>Name</label>
                                 <input class="large-input"
                                     value=${payload?.app_data_sections?.business_section?.call_center_name} />
                             </div>
                             <div class="eight wide field">
                                 <label>Phone</label>
                                 <input class="large-input" />
                             </div>
                         </div>
                         <div class="fields">
                             <div class="ten wide field">
                                 <label>Merchant Legal BusinessƒDBA Name </label>
                                 <input class="large-input" value=${payload?.app_data_sections?.dba_section?.dba_name} />
                             </div>
                             <div class="five wide field">
                                 <label>Date</label>
                                 <input class="large-input" />
                                 <span class="date-icon"></span>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <!-- Note the anchor tag for the signature field is in white. -->
         <h3 style="margin-top:3em;">Signature : <span style="color:white;">/sn1/</span></h3>
     </body>
     
     </html>`
 }
 