import { Router, Request, Response } from "express";
const router = Router();

import pdf from 'html-pdf';
const pdfTemplate = require('./details.js');

const path = require('path')
const demoDocsPath = path.resolve(__dirname, '../../files')

router.post("/generate_pdf", async (req: Request, res: Response) => {
    try {
        const payload = req.body;
        const fileName = `${new Date().getTime()}.pdf`;

        console.log('payload : ', payload);
        pdf.create(pdfTemplate(payload), {format: 'A4'}).toFile(path.resolve(demoDocsPath, fileName), function (err, res) {
            if (err) return console.log(err);
        });
        return res.status(200).json({'message': 'Successfully Created PDF!!', 'filePath': path.resolve(demoDocsPath, fileName)});
    }
    catch (err) {
        console.log("err : ", err);
        return res.status(500).json({'message': 'Server is not responding'});
    }
})

export default router;
