import { Router, Request, Response } from "express";
const router = Router();

import axios from 'axios';
import xml2js from 'xml2js';

// var parseString = require('xml2js').parseString;

const axiosInstance = axios.create({
    headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
    }
});


router.post("/validate_address", async (req: Request, res: Response) => {
    try {
        const { address } = req.body;

        const axiosResp = await axios.get(`http://production.shippingapis.com/ShippingAPI.dll?API=Verify &XML=<AddressValidateRequest USERID="789INCEN2273">
            <Address ID="1">
                <Address1>${address.address1}</Address1>
                <Address2>${address.address2}</Address2>
                <City>${address.city}</City> 
                <State>${address.state}</State>
                <Zip5>${address.zip5}</Zip5>
                <Zip4>${address.zip4}</Zip4>
            </Address> 
        </AddressValidateRequest>`);

        let xmlParsedData = await xml2js.parseStringPromise(axiosResp.data);

        let message: string;
        if (xmlParsedData.AddressValidateResponse.Address[0].Error) {
            message = xmlParsedData.AddressValidateResponse.Address[0].Error[0].Description[0];
        } else {
            message = `Address is correct !`;
        }

        return res.status(200).json({ message });
    }
    catch (err) {
        return res.status(500).json({
            'message': 'Server is not responding'
        });
    }
})

export default router;
