import { Application } from "express"
import testRouter from "./test/routes"
import pdfGenerator from "./pdfGen/routes"
import uspsRouter from "./usps/routes"
import docuSignRouter from "./docuSign/routes"

export const routesConfig = (app: Application) => {
  app.use("/test", testRouter);
  app.use("/pdf_gen", pdfGenerator);
  app.use("/usps", uspsRouter);
  app.use("/docusign", docuSignRouter);
}