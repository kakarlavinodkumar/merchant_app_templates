// import express from 'express';

// const app = express();
// const port = 3000;

// app.get('/', (req, res) => {
//     res.send('Server is running!');
// });

// app.get('/generate_pdf', (req, res) => {
//     console.log("in generate pdf file!");
// })

// app.listen(port, () => {
//     return console.log(`server is listening on ${port}`);
// });


import express from "express";
import cors from 'cors';
import bodyParser from "body-parser";
import compression from 'compression';
import cookieParser from 'cookie-parser';

import { routesConfig } from "./routes"

const app: express.Application = express();
app.use(cors({ origin: true }));
app.use(compression());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

routesConfig(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    let err = new Error('Not Found');
    return res.status(404).json({message : "No route found."});
});

let httpServer = require('http').createServer(app);
httpServer.listen(3030, function() {
    console.log('Unified POC Backend is Running on ' + 3030 + '.');
});

httpServer.timeout = 120000;
module.exports = app;